#!/usr/bin/env python

import os, errno
import sys
import time
import tarfile

import config

base_directory = config.ROOT_BACKUP_DIR
current_backup_dir = "%s/%s" % (base_directory, time.strftime(config.DIRNAME_STRUCTURE_PATTERN))
log_file_path = "%s/%s" % (config.LOG_DIR, config.LOG_FILE)
init_file_path = "%spyautobackup" % config.INIT_RC_DIR

DATABASE_NAME, DATABASE_USERNAME, DATABASE_PASSWORD = 0,1,2


def pyautobackup_info():
    print "-- Welcome to PyAutobackup application --"
    print "                 --  --"


def file_rotate():
    pass


def clean_plain_files():
    list = os.popen("find %s -name *.plain" % current_backup_dir).readline()
    for file in list.split('\n'):
        print file
        if os.path.isfile(file): os.remove(file)
    append_to_log("Cleaning %s directory..." % current_backup_dir, True)


def create_postgresql_backups():
    pgsql_databases_founded = len(config.backup_postgresql_databases)
    append_to_log("%d POSTGRESQL database(s) found to backup..." % pgsql_databases_founded, True)

    for database in config.backup_postgresql_databases:
        if len(database) < 3:
            print "Config file mal formed!, check config.py file"
            print "Exiting without changes"
            return None

        print "backing up database %s[user=%s,password=*****]" % (database[DATABASE_NAME], database[DATABASE_USERNAME])

        mysqldump_command = "PGPASSWORD='%s' pg_dump -i -h localhost -p 5432 -U %s -F c -b -v %s" \
            % (database[DATABASE_PASSWORD], database[DATABASE_USERNAME], database[DATABASE_NAME])
        sql_out = os.popen(mysqldump_command).read()

        backup_file_path = "%s/%s.sql.plain" % (current_backup_dir, database[DATABASE_NAME])
        tar_backup_file_path = "%s/%s.sql.tar.gz" % (current_backup_dir, database[DATABASE_NAME])

        if not os.path.exists(backup_file_path):
            sql_file = open(backup_file_path, "w")
            sql_file.write(sql_out)
            sql_file.close()

            append_to_log("* %s plain SQL file created!" % backup_file_path, True)

            append_to_targz(tar_backup_file_path, backup_file_path)

            append_to_log("* %s compressed SQL file created!" % backup_file_path, True)
        else:
            append_to_log("*** WARNING: %s database sql file exist!" % backup_file_path, True)


def create_mysql_backups():
    msql_databases_founded = len(config.backup_mysql_databases)
    append_to_log("%d MYSQL database(s) found to backup..." % msql_databases_founded, True)

    for database in config.backup_mysql_databases:
        if len(database) < 3:
            print "Config file mal formed!, check config.py file"
            print "Exiting without changes"
            return None

        print "backing up database %s[user=%s,password=*****]" % (database[DATABASE_NAME], database[DATABASE_USERNAME])

        mysqldump_command = "mysqldump -h localhost --user='%s' --password='%s' %s" % (
            database[DATABASE_USERNAME], database[DATABASE_PASSWORD], database[DATABASE_NAME])
        sql_out = os.popen(mysqldump_command).read()

        backup_file_path = "%s/%s.sql.plain" % (current_backup_dir, database[DATABASE_NAME])
        tar_backup_file_path = "%s/%s.sql.tar.gz" % (current_backup_dir, database[DATABASE_NAME])

        if not os.path.exists(backup_file_path):
            sql_file = open(backup_file_path, "w")
            sql_file.write(sql_out)
            sql_file.close()

            append_to_log("* %s plain SQL file created!" % backup_file_path, True)

            append_to_targz(tar_backup_file_path, backup_file_path)

            append_to_log("* %s compressed SQL file created!" % backup_file_path, True)
        else:
            append_to_log("*** WARNING: %s database sql file exist!" % backup_file_path, True)


def create_directories_backups():
    directories_founded = len(config.backup_file_directories)
    append_to_log("%d directories(s) found to backup..." % directories_founded, True)


def append_to_targz(newfile, file_to_append):
    print file_to_append
    with tarfile.open(newfile, "w:gz") as tar:
        tar.add(file_to_append)


def create_databases_backups():
    create_postgresql_backups()
    create_mysql_backups()


def create_databases_directories():
    directories_founded = len(config.backup_mysql_databases)
    append_to_log("%d directories(s) found to backup..." % directories_founded, True)


##
# Verifica si existe la estructura de archivos de pyautobackup.
#
def create_pyautobackup_file_structure():
    if not os.path.isdir(base_directory):
        print " * base directory [%s] not exist...creating." % base_directory
        mkdir_p(base_directory)
        append_to_log("%s directory created" % base_directory, True)

    if not os.path.isdir(current_backup_dir):
        print " * current backup directory [%s] not exist...creating." % current_backup_dir
        mkdir_p(current_backup_dir)
        append_to_log("%s backup directory created" % current_backup_dir, True)


##
# Guarda informacion de depuracion y de eventos en la bitacora de la aplicacion. Si el
# archivo no existe lo crea.
#
def append_to_log(message, to_screen = True):
    if not os.path.exists(log_file_path):
        print "* log file [%s] not exist... creating." % log_file_path
        mkdir_p(config.LOG_DIR)

    if to_screen:
        print message

    log = open(log_file_path, "a")
    log.write(message + "\n")


def mkdir_p(path):
    try:
        os.makedirs(path)
    except OSError as exc: # Python >2.5
        if exc.errno == errno.EEXIST and os.path.isdir(path):
            pass
        else: raise


def status():
    print "STATUS not implemented yet!"


def make_backup():
    pyautobackup_info()
    append_to_log("PyAutoBackup invoked %s..." % time.strftime("%Y%m%d %H:%M"), True)
    create_pyautobackup_file_structure()
    create_databases_backups()
    create_databases_directories()
    clean_plain_files()


def is_installed():
    return os.path.islink(init_file_path)


def make_install():
    if is_installed():
        print "** PyAutoBackup is ALREADY INSTALLED!"
    else:
        os.symlink(os.path.abspath(__file__), init_file_path)
        print "** PyAutoBackup was SUCESSFUL INSTALLED!"


def make_uninstall():
    if not is_installed():
        print "** PyAutoBackup is NOT INSTALLED YET!"
    else:
        os.remove(init_file_path)
        print "** PyAutoBackup was SUCCESFUL UNINSTALLED!"


def error_usage():
	print "Usage: %s {install|uninstall|backup|status}" % sys.argv[0]


if __name__ == '__main__':
    if not os.geteuid() == 0:
        sys.exit("\n** Only root can run this script, bye!\n")

    options = {
        'backup': make_backup,
        'install': make_install,
        'uninstall': make_uninstall,
        'status': status
    }

    if len(sys.argv) < 2:
        error_usage()
    else:
        action = sys.argv[1]

        try:
            do = options[str(sys.argv[1])]
            do()
        except KeyError:
            error_usage()