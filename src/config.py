
backup_mysql_databases = [
    ("sisgemed", "root", "root")
]
backup_postgresql_databases = []
backup_file_directories = [
    "/etc/mysql"
]

#
# A partir de aqui modifique solo si sabe lo que hace
# --------------------------------------------
ROOT_BACKUP_DIR = "/var/backups/pyautobackup/local"
OS_SEPARATOR = "/"
DIRNAME_STRUCTURE_PATTERN = "%Y/%m/%d"

FILE_BACKUP_STRUCTURE = "%s.%s"

LOG_DIR = "/var/log/pyautobackup/"
LOG_FILE = "pyautobackup.log"

INIT_RC_DIR = "/etc/cron.daily/"
